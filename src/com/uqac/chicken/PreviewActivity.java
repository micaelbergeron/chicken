package com.uqac.chicken;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: micael
 * Date: 22/03/13
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class PreviewActivity extends Activity {
    public static final String EXTRA_TEXT = "com.uqac.chicken.preview.text";
    static final String MAGIC_START = "$START$";
    static final String MAGIC_END = "$END$";

    String text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        text = getIntent().getStringExtra(EXTRA_TEXT) ;
        if (text == null) {
            // simulate a back press
            onBackPressed();
        } else {
            try {
                BitmapDrawable secret = embedTextInImage(text, R.drawable.yellow_potato);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // The string will be embeded as $START$<string>$END$
    private BitmapDrawable embedTextInImage(String text, int imageId)
        throws Exception
    {
        // try to load the image
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), imageId);

        if (bmp.isMutable()) {
            // validate the string length
            final int MAX_LENGTH = bmp.getHeight() * bmp.getWidth() - MAGIC_START.length() - MAGIC_END.length();
            if (text.length() > MAX_LENGTH) throw new Exception("The text is too long for the image.");

            String to_embed = MAGIC_START + text + MAGIC_END;
            for (char c : to_embed.toCharArray()) {
                // do the embed logic here
            }

        }

        return new BitmapDrawable(getResources(), bmp);
    }

}