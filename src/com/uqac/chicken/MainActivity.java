package com.uqac.chicken;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final EditText txtText = (EditText)findViewById(R.id.text);
        Button btnPreview = (Button)findViewById(R.id.btnNext);

        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent e = new Intent(MainActivity.this, PreviewActivity.class);
                e.putExtra(PreviewActivity.EXTRA_TEXT, txtText.getText());
                startActivity(e);
            }
        });
    }


}
